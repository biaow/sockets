/**
 * Created by asus on 2017/6/10.
 */
import java.util.ArrayList;
import java.util.List;


public class Front {
    public List<String> front;


    public Front(int num, String word) {
        front = new ArrayList<String>();
        for(int i=0; i<num; i++){
            front.add(word);
        }
    }


   public Front(Front prefix) {
       this.front = new ArrayList<String>(prefix.front);
    }

    private static final int MULTIPLIER = 31;
    public int hashCode(){
        int h = 0;
        for(int i = 0; i< front.size(); i++){
            h = h*MULTIPLIER + front.get(i).hashCode();
        }
        return h;
    }


    public boolean equals(Object o){
        Front p = (Front)o;
        for(int i = 0; i< front.size(); i++){
            if(!front.get(i).equals(p.front.get(i))){
                return false;
            }
        }
        return true;
    }

}
