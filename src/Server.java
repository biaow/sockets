/**
 * Created by asus on 2017/6/7.
 */
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
    public static final int PORT = 12345;//监听的端口号

    public static void main(String[] args){
        System.out.println("服务器启动...\n");
        Server server = new Server();
        server.init();
    }


    public void init() {
        try {
            ServerSocket serverSocket = new ServerSocket(PORT);
            while (true) {
                // 一旦有堵塞, 则表示服务器与客户端获得了连接
                Socket client = serverSocket.accept();
                // 处理这次连接
                new HandlerThread(client);
            }
        } catch (Exception e) {
            System.out.println("服务器异常: " + e.getMessage());
        }
    }
        private class HandlerThread implements Runnable {
            private Socket socket;

            public HandlerThread(Socket client) {
                socket = client;
                new Thread(this).start();
            }

            public void run() {
                try {
                    // 读取客户端数据
                    DataInputStream input = new DataInputStream(socket.getInputStream());
                    byte [] bytes = new byte[100];
                    input.read(bytes);
                    FileInputStream f2 = new FileInputStream("key1.dat");
                    int num2=f2.available();
                    byte [] num = new byte[num2];
                    f2.read(num);
                    SecretKeySpec k =new SecretKeySpec(num,"DESede");
                    Cipher co = Cipher.getInstance("DESede");
                    co.init(Cipher.DECRYPT_MODE,k);
                    byte []pte=co.doFinal();
                    String clientInputStr = new String(bytes);
                    String P = new String(pte,"UTF8");
                    System.out.print(P);
                    // 处理客户端数据
                    System.out.println("客户端发过来的内容:" );
                    // 向客户端回复信息
                    DataOutputStream out = new DataOutputStream(socket.getOutputStream());
                    if (clientInputStr.equals("NO")) {
                        out.writeUTF("OK");
                    } else {
                        MyDC myDC = new MyDC();
                       // String m = String.valueOf(myDC.evaluate(screat(bytes)));
                        //out.writeUTF(m);
                    }
                    //System.out.print("请输入:\t");
                    // 发送键盘输入的一行
                    //String s = new BufferedReader(new InputStreamReader(System.in)).readLine();


                    out.close();
                    input.close();
                } catch (Exception e) {
                    System.out.println("服务器 run 异常: " + e.getMessage());
                } finally {
                    if (socket != null) {
                        try {
                            socket.close();
                        } catch (Exception e) {
                            socket = null;
                            System.out.println("服务端 finally 异常:" + e.getMessage());
                        }
                    }
                }
            }
        }
    }

